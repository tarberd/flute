from conans import ConanFile, CMake


class fluteConan(ConanFile):
    name = "flute"
    version = "1.0"
    license = "BSD"
    author = "Bernardo Ferrari Mendonca bernardo.mferrari@gmail.com"
    url = "https://gitlab.com/tarberd/flute"
    description = "Rectilinear Steiner minimal tree and wirelength estimation algorithm."
    topics = ("flute", "eda")
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False]}
    default_options = "shared=False"
    generators = "cmake"
    exports_sources = ["extension/*",
        "CMakeLists.txt",
        "Readme",
        "PORT9.dat",
        "POWV9.dat",
        "flute*",
        "license.txt" ]

    def build(self):
        cmake = CMake(self)
        cmake.configure()
        cmake.build()

    def package(self):
        cmake = CMake(self)
        cmake.configure()
        cmake.install()

    def package_info(self):
        self.env_info.FLUTE_LUT_PATH = self.package_folder + "/share/flute"
